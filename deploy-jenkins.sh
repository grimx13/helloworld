#!/bin/bash
# Deploy via Jenkins to minikube

export SERVICE="${1}"
export TAG="${2}"

if [ -z "$SERVICE" ]; then
  echo "Error: Deploy requires parameters for SERVICE and TAG"
  exit 1
fi

echo "Deploying application ${SERVICE} version ${TAG} to minikube:"
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${SERVICE}
  labels:
    app: ${SERVICE}
spec:
  replicas: 3
  selector:
    matchLabels:
      app: ${SERVICE}
  template:
    metadata:
      labels:
        app: ${SERVICE}
    spec:
      containers:
      - name: ${SERVICE}
        image: pwdebruin/${SERVICE}:${TAG}
EOF

echo "Exposing application ${SERVICE}:"
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Service
metadata:
  name: ${SERVICE}
spec:
  selector:
    app: ${SERVICE}
  ports:
  - protocol: TCP
    port: 80
    targetPort: 9000
  type: LoadBalancer
EOF

echo "Tracking rollout for application ${SERVICE} version ${TAG}:"
kubectl rollout status deploy/${SERVICE} --watch
