node {
    properties([
        parameters([
            string(
                name: 'SERVICE',
                defaultValue: 'helloworld',
                description: 'Enter name of application you would like to build and deploy.'
            )
        ])
    ])
    stage('Checkout') { 
        git 'https://gitlab.com/grimx13/helloworld.git'
    }
    stage('Build') {
        app = docker.build("pwdebruin/helloworld")
    }
    stage('Test') {
        app.inside {
            sh '''
            # Start app in background
            app --title "Testing" --message "Success" 2> /dev/null &
            # Wait for app to startup
            sleep 5
            # Check for app success response in HTML via curl
            curl -s localhost:9000 | grep "Success"
            '''
        }
    }
    stage('Push image') {
        docker.withRegistry('https://registry.hub.docker.com', 'docker-hub-credentials') {
            app.push("${env.BUILD_NUMBER}")
            app.push("latest")
        }
    }
    stage('Deploy') {
        sh '''
        ./deploy-jenkins.sh $SERVICE $BUILD_NUMBER 
        '''
    }
}
