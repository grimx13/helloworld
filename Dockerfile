FROM golang:1.8-alpine AS builder

WORKDIR /usr/src/app

COPY . .

ENV GOPATH="/usr/src/app/"
RUN apk --no-cache add git

RUN cd src/app \
  && go-wrapper download \
  && go build -v 

FROM alpine:3.5

RUN apk --no-cache add ca-certificates
RUN apk --no-cache add curl
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

WORKDIR /usr/local/bin
EXPOSE 9000

COPY --from=builder /usr/src/app/src/app .

USER appuser
CMD ["app", "--title", "Hello", "--message", "Hello World!"]
