# helloworld
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/grimx13/helloworld)](https://goreportcard.com/report/gitlab.com/grimx13/helloworld)

### This project contains:
- Golang web application
- Docker build
- Kubernetes deployment manifests

### Dependencies
Although a installation script to bootstrap minikube on a MacOs machine will be
provided, the project requires the following to deploy:
- kubectl with kubeconf context configured
- Docker running if you wish to rebuild a container from the Dockerfile
- Docker registry to push your image to before deploying to kubernetes

## Golang Application
The application accepts command line flag values which sets a custom HTML title
and message that will be displayed when visting the endpoint.

The application uses build in Golang packages except for one [third party package](https://github.com/gorilla/handlers)
which is used for access logs from the web server.

```
Usage:
app --title=<title> --message=<message>

Flag:          Discription:
--message      Custom H1 HTML message
--title        Custom HTML title
```

## Dockerfile
The Dockerfile builds the Golang app in a alpine Golang builder container, and
add the binary to a lightweight alpine container for execution. This improves 
image pull times and reduces the maintenance that comes with vulnerability 
management of outdated packages.

To build a new container:
```
docker build -t <optional registry>/<image name>:<image tag>
```

## Kubernetes manifests
The deployment and service yaml contains typical stateless application definitions
running two replicas and exposing the port of the Golang application.

Its default configuration runs three replicas, and exposes the deployment with
a service of type LoadBalancer discovering pods with a selector label:
`app: helloworld`

To deploy the application to a existing kubernetes cluster or minikube run:
```
./deploy.sh
```
or 
```
kubectl apply -f kubernetes/deployment.yaml
kubectl apply -f kubernetes/service.yaml

```

## Install minikube
In order to install minikube you need to run a compatible vm-driver or you can
run it on the host itself although unrecommended.

You can run minikube on MacOs by doing the following (requires Docker):

```
./install-minikube.sh
```

OR

1. Install the dependencies:

    1.1 Install [Homebrew](https://brew.sh/) and run the below to install cask:
    ```
    brew tap caskroom/cask
    ```
    1.2 Install Docker for mac by following the instructions here: 
    https://docs.docker.com/docker-for-mac/install/

    1.3 Install VirtualBox to use as a vm driver
    ```
    brew cask install virtualbox
    ```

    1.4 Install kubectl:
    ```
    brew install kubectl
    ```
    

2. Install minikube
```
brew cask install minikube
```

3. Start minikube
```
minikube start
```
