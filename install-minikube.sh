#!/bin/bash
# Install minikube on MacOs

function brew-install {
  read -n1 -p "Would you like to install $1? (y/n) " INSTALL

  if [ "$INSTALL" != "y" ]; then
    echo "Installation refused by user"
    exit 0
  fi

  if [ "$dep" == "kubectl" ]; then
    brew install $1
  else
    brew cask install $1
  fi
}

# Check if MacOs
if [[ ! "$OSTYPE" == "darwin"* ]]; then
  echo "error: This script only supports execution on MacOs"
  exit 1
fi

# Check if homebrew is installed
if [ ! $(which brew || false) ]; then
  echo "error: Homebrew not installed"
  echo "Follow installation instructions at http://brew.sh"
  exit 1
fi

# Check if dependencies are installed
DEPENDENCIES="kubectl virtualbox minikube"

for dep in $DEPENDENCIES; do
  echo -en "Checking if $dep is installed \t"
  if [ ! $(which $dep || false) ]; then
    echo "NO"
    brew-install $dep
  else
    echo "YES"
  fi
done
