#!/bin/bash
# Deploy to minikube

# Ensure minikube is started
if [ "$(which minikube || false)" ]; then
  echo "Starting minikube and configure kubectl context"
  minikube start
  echo " "
else
  echo "error: minikube not installed or exported to env PATH"
  echo "Run ./install-minikube.sh first"
  exit 1
fi

# Deploy with kubectl
if [ "$(which kubectl || false)" ]; then
  echo "Deploying application to minikube:"
	kubectl apply -f kubernetes/deployment.yaml
	kubectl apply -f kubernetes/service.yaml
  echo " "
else
	echo "error: kubectl not installed or exported to env PATH"
  echo "Run ./install-minikube.sh first"
  exit 1
fi

# Check if the deployment rollout is complete
echo "Check deployment rollout status"
kubectl rollout status deploy helloworld --watch
echo " "

# Open web application in browser
echo "Launch application in browser"
minikube service helloworld
