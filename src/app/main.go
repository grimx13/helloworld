package main

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/gorilla/handlers"
	"html/template"
	"log"
	"net/http"
	"os"
)

const helloworldTpl = `<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{{.Title}}</title>
  </head>
  <body>
    <h1>{{.Message}}</h1>
  </body>
</html>`

type data struct {
	Title, Message string
}

func parseTemplate(title, message *string) (string, error) {
	var d data

	log.Printf("Parsing html template")
	t, err := template.New("page").Parse(helloworldTpl)
	if err != nil {
		return "", err
	}

	d.Title, d.Message = *title, *message

	log.Printf("Substitute values on template")
	content := new(bytes.Buffer)
	err = t.Execute(content, d)
	if err != nil {
		return "", err
	}

	return content.String(), nil
}

func httpServeForever(content string) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, content)
	})

	log.Printf("Starting http server")
	http.ListenAndServe(":9000", handlers.LoggingHandler(os.Stdout, http.DefaultServeMux))
}

func main() {
	log.Printf("Starting application")

	title := flag.String("title", "hello world", "Page Title")
	message := flag.String("message", "hello world", "Page Message")
	flag.Parse()

	content, err := parseTemplate(title, message)
	if err != nil {
		log.Fatal(err)
	}

	httpServeForever(content)
}
